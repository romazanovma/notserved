from django.db import models
from model_utils.models import TimeStampedModel


class Notification(models.Model):
    start_time = models.DateTimeField(verbose_name="Start time", blank=False, null=False)
    message_text = models.CharField(max_length=2048, verbose_name="Text", blank=False, null=False)
    client_filter = models.CharField(max_length=24, verbose_name="Filter", blank=False, null=False)
    end_time = models.DateTimeField(verbose_name="End time", blank=False, null=False)


class Client(models.Model):
    phone_number = models.IntegerField(verbose_name="Phone number", blank=False, null=False)
    operator_code = models.CharField(max_length=3, verbose_name="Operator code", blank=False, null=False)
    tag = models.CharField(max_length=24, verbose_name="Tag", blank=True, null=False)
    time_zone = models.IntegerField(verbose_name="Time zone", blank=False, null=False, default=0)


class Message(TimeStampedModel):
    PENDING = "PENDING"
    ERROR = "ERROR"
    DONE = "DONE"
    STATUS_LIST = (
        (PENDING, PENDING),
        (ERROR, ERROR),
        (DONE, DONE)
    )
    status = models.CharField(max_length=16, verbose_name="Status", choices=STATUS_LIST, default=PENDING)
    notification = models.ForeignKey(Notification, verbose_name="Notification", related_name="messages",
                                     blank=False, null=False, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, verbose_name="Client", related_name="messages", blank=False,
                               null=False, on_delete=models.CASCADE)