from django.urls import path

from . import views

urlpatterns = [
    path('', views.notification_index, name='notification index'),
    path('notification_list/notification/<int:notification_id>', views.notification_details, name='notification details'),
    path('notification_list', views.get_notification_list, name='notifications'),
    path('client_list/client/<int:client_id>', views.client_details, name='client details'),
    path('client_list', views.get_client_list, name='clients'),
    path('create_client', views.create_client, name='create client'),
    path('create_notification', views.create_notification, name='create notification'),
]