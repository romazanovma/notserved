from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse

from notification_server.models import Client, Notification
from notserved import settings


def index(request):
    """Главная страница со ссылкой на notification index"""
    return HttpResponse("Notserved index page")


def notification_index(request):
    """основная страница со ссылками на: список клиентов, список рассылок,
        добавление клиента, добавление рассылки"""

    links = {"client_list": reverse(get_client_list),
             "create_client": reverse(create_client),
             "notification_list": reverse(get_notification_list),
             "create_notification": reverse(create_notification)}
    context = {"links": links}
    return render(request, 'notification_index.html', context)


def get_client_list(request):
    """Список клиентов"""
    clients = Client.objects.all().order_by('id')
    paginator = Paginator(clients, 50)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'client_list.html', {'page_obj': page_obj})


def client_details(request, client_id):
    client = get_object_or_404(Client, pk=client_id)
    return render(request, 'client_details.html', {'client': client})


def get_notification_list(request):
    """Список рассылок"""
    notifications = Notification.objects.all().order_by('id')
    paginator = Paginator(notifications, 50)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'notification_list.html', {'page_obj': page_obj})


def notification_details(request, notification_id):
    notification = get_object_or_404(Client, pk=notification_id)
    return render(request, 'notification_details.html', {'client': notification})


def create_client(request):
    return HttpResponse("Notserved create client")


def create_notification(request):
    return HttpResponse("Notserved create notification")
